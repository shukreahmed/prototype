       //
//  PostFOrData.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/18/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import Foundation
class PostFOrData {
    // the completion closure signature is (NSString) -> ()
    var product_name:String!
    var apiKey:String!
    var lat:String!
    var long:String!
    var radius:String!
    var priceRange:String!
    var goldenUrl:String!
    
    init (product_name: String, apiKey: String, lat: String, long:String, radius:String, priceRange:String) {
        self.product_name = product_name
        self.apiKey = apiKey
        self.lat = lat
        self.long = long
        self.radius = radius
        self.priceRange = priceRange
        self.goldenUrl = "https://api.goodzer.com/products/v0.1/search_stores/?query=" + product_name + "&" + "lat=" + lat + "&" + "lng=" + long + "&" + "radius=" + radius + "&" + "priceRange=" + priceRange + "&" + "apiKey=" + apiKey
    }
    
    init (url : String){
        self.goldenUrl = url
    }
    
    
    func forData(completion: @escaping (AnyObject) -> ()) {
        if let url = URL(string: goldenUrl) {
            let request = NSMutableURLRequest( url: url)
            request.httpMethod = "POST"
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                if let urlcontent = data {
                    do {
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: urlcontent, options: JSONSerialization.ReadingOptions.mutableContainers)
//                        print(jsonResult)
                        completion(jsonResult as AnyObject)
                    } catch {
                        print("JSON SERIALIZATION FAILED")
                    }
                }}
            task.resume()
        }
    }
    
    func googleData(_ completion : (AnyObject) -> ()) {
        
    }

}

    
