//
//  FirstView.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/16/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import Foundation

class FirstView: UIViewController {
    override func viewDidLoad() {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
}