//
//  TableViewController.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/16/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import UIKit
import CoreLocation

class TableViewController: UITableViewController{

    var items = [""]
    var product_name:String!
    var passedStores = [Store]()
    var distances = [String]()
    var pro:String!

//    var product_names = [String]()
//    var product_image_urls = [String]()
//    var store_names = [String]()
//    var product_urls = [String]()

    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return passedStores.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! cellTableViewCell
        requestImage(passedStores[(indexPath as NSIndexPath).row].product_url) { (image) -> Void in
            let current_store = self.passedStores[(indexPath as NSIndexPath).row]
            cell.imageView?.image = image
            cell.label.text = current_store.product_name
            cell.storeName.text = current_store.store_name
            cell.productPrice.text = "$" + String(current_store.product_price)
            cell.productDistance.text = self.distances[current_store.jsonIndex]
            cell.setNeedsLayout()
        }
        
        
        return cell
    }
    
    func requestImage(_ url: String, success: @escaping (UIImage?) -> Void) {
        requestURL(url, success: { (data) -> Void in
            if let d = data {
                success(UIImage(data: d))
            }
        })
        }
    }
    
//    func requestURL(url: String, success: (NSData?) -> Void, error: ((NSError) -> Void)? = nil) {
//        NSURLConnection.sendAsynchronousRequest(
//            NSURLRequest(URL: NSURL (string: url)!),
//            queue: NSOperationQueue.mainQueue(),
//            completionHandler: { response, data, err in
//                if let e = err {
//                    error?(e)
//                } else {
//                    success(data)
//                }
//        })
//    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
