//
//  Store.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/19/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import Foundation
class Store {
    var store_name:String!
    var product_name:String!
    var product_price:Double!
    var product_url:String!
    var store_lat:String!
    var store_long:String!
    var distance:String!
    var distanceDouble:Double!
    var jsonIndex:Int!
    init(store_name : String){
        self.store_name = store_name
    }
}