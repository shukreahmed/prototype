//
//  ActivityViewController.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/19/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import Foundation
import CoreLocation
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class ActivityViewController : UIViewController {
    var product_name:String!
    var distances = [String]()
    var location:CLLocationManager!
    var apiKey = "1eb2fbec37c70e8dbf476eb136b330f7"
    var googleApiKey = "AIzaSyBZMr4-owo5j44-0mfQnk5G6rU_5pMqOc4"
    var lat:String!
    var long:String!
    var radius = "1"
    var priceRange = "0:60"
    var all_stores = [Store]()
    var jsonRespone:NSString!
    @IBOutlet weak var locationShow: UILabel!
    
    @IBOutlet weak var busySpinner: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.busySpinner.startAnimating()
        self.navigationItem.prompt = ""
        let image = UIImage(named: "Inventory_logo.png")
        self.navigationItem.titleView = UIImageView(image: image)
        let leftButton =  UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: "barButtonPressed:")
        let rightButton = UIBarButtonItem(title: "Right Button", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        leftButton.image = UIImage(named:"tabButton.png")
        rightButton.image = UIImage(named: "boxImage.png")
        navigationItem.leftBarButtonItem = leftButton
//        print(location.location)
//        print(location.location?.coordinate.latitude))
        lat = String(Double((location.location?.coordinate.latitude)!))
        long = String(Double((location.location?.coordinate.longitude)!))
//        locationShow.text = "lat:" + lat + ", long:" + long
    }
    override func viewDidAppear(_ animated: Bool) {
        let semaphore = DispatchSemaphore(value: 0)
        let pfd = PostFOrData(product_name: self.product_name, apiKey: self.apiKey, lat: self.lat, long: self.long, radius: self.radius, priceRange: self.priceRange)
        pfd.forData { jsonResult in
            let stores = jsonResult["stores"] as? [[String: AnyObject]]
            for store in stores! {
                let name_id = store["name"] as? String
                let locations = store["locations"] as? NSArray
                
            
                let serializeLAT = locations?.firstObject as? [String:Any]
                
                let latitude = serializeLAT?["lat"] as! NSNumber
                let longitude = serializeLAT?["lng"] as! NSNumber
                let temp_store = Store(store_name: name_id!)
                temp_store.store_lat = String(describing: latitude)
                temp_store.store_long = String(describing: longitude)
                let products = store["products"] as? [[String: AnyObject]]
                for product in products! {
                    let product_url = product["image"] as? String
                    if (product_url != nil){
                        temp_store.product_url = product_url
                        let product_name = product["title"] as? String
                        temp_store.product_name = product_name
                        let product_price = product["price"] as? Double
                        temp_store.product_price = product_price
                        self.all_stores.append(temp_store)
                    }
                }
                
            }
            semaphore.signal()
            
            var visited = [String]()
            var real_visited = [Store]()
            for object in self.all_stores {
                if(!visited.contains(object.product_name)){
                    real_visited.append(object)
                    visited.append(object.product_name)
                }
            }
            self.all_stores = real_visited
            var goldenUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + self.lat + "," + self.long + "&destinations="
            var start = true
            var count = 0;
            for store in self.all_stores {
                if (start) {
                    goldenUrl += store.store_lat
                    start = false
                } else {
                    goldenUrl += "%7C" + store.store_lat
                }
                goldenUrl += "%2C" + store.store_long
                store.jsonIndex = count
                
                count += 1
                
            }
            goldenUrl += "&key=" + self.googleApiKey
            var pfd = PostFOrData(url: goldenUrl)
            pfd.forData { jsonResult in
                let rows = jsonResult["rows"] as? [[String: AnyObject]]
                for row in rows! {
                    let elements = row["elements"] as? NSArray
                    for element in elements! {
                        let test = element as? [String:Any]
                        let testDistance = test?["distance"] as? [String:Any]
                        self.distances.append((testDistance?.first?.value as! NSString) as String)
                        //var dubDub = element["distance"]
                        //var realDub = dubDub!!["text"] as! NSString
                        //self.distances.append(realDub as String)
                    }
                }
                semaphore.signal()
            }
            
            
        }
        semaphore.signal()
        semaphore.wait(timeout: DispatchTime.distantFuture)
        semaphore.wait(timeout: DispatchTime.distantFuture)
        semaphore.wait(timeout: DispatchTime.distantFuture)
        for store in self.all_stores {
            store.distance = distances[store.jsonIndex]
        }
        var max = 0;
        for store in self.all_stores {
            var current = store.distance.replacingOccurrences(of: " mi", with: "")
            var distanceDouble = Double(current)
            store.distanceDouble = distanceDouble!
        }
        self.all_stores = self.all_stores.sorted(by: { $0.distanceDouble < $1.distanceDouble })
        showTable()
        
        
        
        
        /** testing walgreens shit **/
        
    }
    
    func showTable() {
        self.busySpinner.stopAnimating()
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let itemTable: ViewButReallyATable = storyboard.instantiateViewController(withIdentifier: "newViewController") as! ViewButReallyATable
        itemTable.passedStores = self.all_stores
        itemTable.distances = distances
        self.navigationController!.pushViewController(itemTable, animated: true)
    
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}
