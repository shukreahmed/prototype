//
//  TableViewController.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/16/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import UIKit
import CoreLocation

class ViewButReallyATable: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBAction func backButton(_ sender: AnyObject) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let itemTable: SecondView = storyboard.instantiateViewController(withIdentifier: "Search") as! SecondView
        self.present(itemTable, animated: true, completion: nil)


    }
    
    @IBOutlet weak var tableView: UITableView!
    var items = [""]
    var product_name:String!
    var passedStores = [Store]()
    var distances = [String]()
    
    var pro:String!
    
    //    var product_names = [String]()
    //    var product_image_urls = [String]()
    //    var store_names = [String]()
    //    var product_urls = [String]()
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.prompt = ""
        let image = UIImage(named: "Inventory_logo.png")
        self.navigationItem.titleView = UIImageView(image: image)
//        let leftButton =  UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: self, action: "barButtonPressed:")
        let myBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ViewButReallyATable.popToRoot(_:)))
        myBackButton.image = UIImage(named: "backArrow.png")
        navigationItem.leftBarButtonItem  = myBackButton
        let rightButton = UIBarButtonItem(title: "Right Button", style: UIBarButtonItemStyle.plain, target: self, action: nil)
//        leftButton.image = UIImage(named:"tabButton.png")
        rightButton.image = UIImage(named: "map-marker-xxl.png")
//        navigationItem.leftBarButtonItem = leftButton
        navigationItem.rightBarButtonItem = rightButton

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func popToRoot(_ sender:UIBarButtonItem){
        self.navigationController!.popToRootViewController(animated: true)
    }
    
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return passedStores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! cellTableViewCell
        requestImage(passedStores[(indexPath as NSIndexPath).row].product_url) { (image) -> Void in
            let current_store = self.passedStores[(indexPath as NSIndexPath).row]
            cell.imageView?.image = image
            cell.label.text = current_store.product_name
            cell.storeName.text = current_store.store_name
            cell.productPrice.text = "$" + String(current_store.product_price)
            cell.productDistance.text = self.distances[current_store.jsonIndex]
            cell.setNeedsLayout()
        }
        
        
        return cell
    }
    
    func requestImage(_ url: String, success: @escaping (UIImage?) -> Void) {
        requestURL(url, success: { (data) -> Void in
            if let d = data {
                success(UIImage(data: d))
            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var secondViewController:singleProductView = segue.destination as! singleProductView
    }
}

func requestURL(_ url: String, success: @escaping (Data?) -> Void, error: ((NSError) -> Void)? = nil) {
    NSURLConnection.sendAsynchronousRequest(
        URLRequest(url: URL (string: url)!),
        queue: OperationQueue.main,
        completionHandler: { response, data, err in
            if let e = err {
                error?(e as NSError)
            } else {
                success(data)
            }
    })
}
