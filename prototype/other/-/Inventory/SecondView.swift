//
//  FirstView.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/16/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class SecondView: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate {
    @IBOutlet weak var searchResults: UISearchBar!
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
//        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        locationManager.delegate = self
        self.navigationItem.prompt = ""
        let image = UIImage(named: "Inventory_logo.png")
        self.navigationItem.titleView = UIImageView(image: image)
        let leftButton =  UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        let rightButton = UIBarButtonItem(title: "Right Button", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        leftButton.image = UIImage(named:"tabButton.png")
        rightButton.image = UIImage(named: "boxImage.png")
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.rightBarButtonItem = rightButton
        
        let searchImage: UIImage = UIImage(named: "searchMagnifier.png")!
        self.searchResults.setImage(searchImage, for: UISearchBarIcon.search, state: UIControlState())
        //to change placeholder size
        for subView in searchResults.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("Search for any item", comment:""),
                                                                          attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName : UIFont(name: "HelveticaNeue-Thin", size: 18)!])
                }
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondViewController:ActivityViewController = segue.destination as! ActivityViewController
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        secondViewController.product_name = searchResults.text?.replacingOccurrences(of: " ", with: "+")
        secondViewController.location = locationManager
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
    }
//    override func viewDidLayoutSubviews() {
//        self.navigationController?.navigationBar.translucent = false
//    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let suggestationTable: itemSuggestions = storyboard.instantiateViewControllerWithIdentifier("itemSuggestions") as! itemSuggestions
//        self.presentViewController(itemSuggestions, animated: true, completion: nil)
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let itemTable: itemSuggestions = storyboard.instantiateViewController(withIdentifier: "itemSuggestions") as! itemSuggestions
//        let navigationBarAppearance = itemTable.navigationController?.navigationBar
//        navigationBarAppearance!.barTintColor = uicolorFromHex(0x161C31)
//        navigationBarAppearance!.translucent = false
//        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
//        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        itemTable.navigationItem.prompt = ""
        let image = UIImage(named: "Inventory_logo.png")
        itemTable.navigationItem.titleView = UIImageView(image: image)
        let leftButton =  UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        let rightButton = UIBarButtonItem(title: "Right Button", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        leftButton.image = UIImage(named:"tabButton.png")
        rightButton.image = UIImage(named: "boxImage.png")
        itemTable.navigationItem.leftBarButtonItem = leftButton
        itemTable.navigationItem.rightBarButtonItem = rightButton
//        self.presentViewController(itemTable, animated: true, completion: nil)
        self.navigationController!.pushViewController(itemTable, animated: true)
    }
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
}
