//
//  ViewController.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/15/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class ViewController: UIViewController, CLLocationManagerDelegate  {

//    var locationManage:CLLocationManager!
//    var lat:String!
//    var long:String!
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
//    @IBAction func signUp(sender: AnyObject) {
//        if username.text == "" || password.text == "" {
//            var alert = UIAlertController(title: "Error in form", message: "Please enter your email and password", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction((UIAlertAction(title: "OK", style: .Default, handler: { (action) in
//                self.dismissViewControllerAnimated(true, completion: nil)
//            })))
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else {
//            self.performSegueWithIdentifier("login" ,sender: self)
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        Alamofire.request(.GET, "")
//            .responseJSON { response in
//                if let JSON = response.result.value {
//                    print(JSON)
//                }
//        }
//        Alamofire.request(.GET, "")
//            .responseJSON { response in
//                if let JSON = response.result.value {
//                    print(JSON)
//                }
//        }
        Alamofire.request("www.cvs.com/search/N-3mZ2k?searchTerm=cups")
            .responseJSON { response in
                if let JSON = response.result.value {
                    print(JSON)
                }
        }
        Alamofire.request("https://www.cvs.com/retail/frontstore/OnlineShopService?apiKey=c9c4a7d0-0a3c-4e88-ae30-ab24d2064e43&apiSecret=4bcd4484-c9f5-4479-a5ac-9e8e2c8ad4b0&appName=CVS_WEB&channelName=WEB&contentZone=resultListZone&deviceToken=7780&deviceType=DESKTOP&lineOfBusiness=RETAIL&navNum=20&operationName=getProductResultList&pageNum=1&referer=http:%2F%2Fwww.cvs.com%2Fsearch%2FN-3mZ2k%3FsearchTerm%3Dcups&serviceCORS=False&serviceName=OnlineShopService&sortBy=relevance&version=1.0")
            .responseJSON { response in
                if let JSON = response.result.value {
                    print(JSON)
                }
        }
        
        
//        locationManage = CLLocationManager()
//        locationManage.delegate = self
//        locationManage.desiredAccuracy = kCLLocationAccuracyBest
//        locationManage.requestWhenInUseAuthorization()
//        locationManage.startUpdatingLocation()
//
//
//        print(long)
//        print(lat)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        let attributes = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName : UIFont(name: "HelveticaNeue-Thin", size: 17)! // Note the !
        ]
        
        username.attributedPlaceholder = NSAttributedString(string: "Username", attributes:attributes)
        password.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributes)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
            else {
                
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
//
//    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [CLLocation]) {
////        var userlocation:CLLocation = locations[0]
////        lat = String(userlocation.coordinate.latitude)
////        long = String(userlocation.coordinate.longitude)
////        print(lat)
////        print(long)
//        print(locations)
//    }
//    @IBAction func loginAction(sender: AnyObject) {
//        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let revealViewController: SWRevealViewController = storyboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
//        self.presentViewController(revealViewController, animated: true, completion: nil)
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

