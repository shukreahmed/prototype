//
//  cellTableViewCell.swift
//  Inventory
//
//  Created by Shukre Ahmed on 7/16/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import UIKit

class cellTableViewCell: UITableViewCell {
    @IBOutlet weak var productImageBro: UIImageView!
    @IBOutlet weak var productDistance: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
//
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

    
}
