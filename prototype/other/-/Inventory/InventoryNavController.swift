//
//  InventoryNavController.swift
//  Inventory
//
//  Created by Shukre Ahmed on 8/23/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

class InventoryNavController: UINavigationController, UIViewControllerTransitioningDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Status bar white font
        self.navigationBar.barStyle = UIBarStyle.black
        self.navigationBar.tintColor = UIColor.white
    }
}
