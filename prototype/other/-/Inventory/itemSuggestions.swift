//
//  itemSuggestions.swift
//  Inventory
//
//  Created by Shukre Ahmed on 8/15/16.
//  Copyright © 2016 Shukre Ahmed. All rights reserved.
//

import Alamofire
import Foundation
import CoreLocation
import UIKit

class itemSuggestions:UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var tableView: UITableView!
    var suggestionsDict = [String]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        self.searchBar.becomeFirstResponder()
        
        self.navigationItem.prompt = ""
        let image = UIImage(named: "Inventory_logo.png")
        self.navigationItem.titleView = UIImageView(image: image)
        let leftButton =  UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: #selector(itemSuggestions.barButtonPressed(_:)))
        let rightButton = UIBarButtonItem(title: "Right Button", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        leftButton.image = UIImage(named:"tabButton.png")
        rightButton.image = UIImage(named: "boxImage.png")
        navigationItem.leftBarButtonItem = leftButton
        
        
        navigationItem.rightBarButtonItem = rightButton
        let searchImage: UIImage = UIImage(named: "searchMagnifier.png")!
        self.searchBar.setImage(searchImage, for: UISearchBarIcon.search, state: UIControlState())
        //to change placeholder size
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString("", comment:""),
                                                                          attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName : UIFont(name: "HelveticaNeue-Thin", size: 18)!])
                }
            }
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        
    }
    
    func barButtonPressed(_ sender: UIBarButtonItem) {
        //perform tab action for the side
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 13 
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "suggestion", for: indexPath) 
        if (self.suggestionsDict.count != 0) {
            if (indexPath as NSIndexPath).row < self.suggestionsDict.count {
                let tempString = self.suggestionsDict[(indexPath as NSIndexPath).row]
                cell.textLabel?.text = tempString
            }
        }
        return cell
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        alamoCall(searchText)
    }
    
    func alamoCall (_ searchText:String) {
        if(searchText != ""){
        var newSearchText = searchText.replacingOccurrences(of: " ", with: "%20")
        Alamofire.request("https://api.indix.com/v2/products/suggestions?countryCode=US&q=" + newSearchText + "&app_id=9b03ba49&app_key=6614ac92f09a67d154307fd865cbd24f")
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let result = JSON as? [String: Any] {
                        if let newResult = result["result"] as? [String:Any]{
                            var tempArray = [String]()
                            for val in newResult{
                                var temp = val.value as! NSArray
                                for check in temp {
                                    if let new_check = check as? [String:Any]{
                                        if(tempArray.count < 13) {
                                            tempArray.append(new_check.values.first as! String)
                                            
                                        }
                                    }
                                    
                                }
                            }
                            self.suggestionsDict = tempArray
                            self.tableView.reloadData()
                            
                            
                        }
                        }
                    /**
                    let result = JSON["result"]
                    var tempArray = [AnyObject]()
                    for val in result{
                        let new_result = val.value as! NSArray
                        for new_new in new_result {
                            let tempDict = new_new as! NSDictionary
                            if(tempArray.count < 13) {
                                tempArray.append(tempDict.allValues)
                            }
                        }
                    }
                    self.suggestionsDict = tempArray
                    self.tableView.reloadData()
 
                **/
                }
            }
        }
    }
    //replace search button and segue related to it in second view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var path = self.tableView.indexPathForSelectedRow
        let indexPath = tableView.indexPathForSelectedRow!
        let currentCell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        searchBar.text = currentCell.textLabel!.text
        let secondViewController:ActivityViewController = segue.destination as! ActivityViewController
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        secondViewController.product_name = searchBar.text?.replacingOccurrences(of: " ", with: "+")
        secondViewController.location = locationManager
    }
}
